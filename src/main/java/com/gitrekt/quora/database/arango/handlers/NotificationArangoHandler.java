package com.gitrekt.quora.database.arango.handlers;

import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDB;
import com.arangodb.ArangoDBException;
import com.arangodb.entity.BaseDocument;
import com.arangodb.util.MapBuilder;
import com.gitrekt.quora.database.arango.ArangoConnection;
import com.gitrekt.quora.models.Notification;
import com.gitrekt.quora.models.Notification.Type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class NotificationArangoHandler extends ArangoHandler<Notification> {
  protected final String dbName;
  protected final ArangoDB connection;

  /** Setting up connection. */
  public NotificationArangoHandler() {
    super("notifications", Notification.class);
    dbName = System.getenv("ARANGO_DB");
    this.connection = ArangoConnection.getInstance().getConnection();
  }

  public Collection<Notification> getNotifications() {
    return super.findAll();
  }

  public Notification getNotification(String key) {
    return super.findOne(key);
  }

  /** Getting the user notifications . */
  public ArrayList<Notification> getUserNotifications(String userId) {
    Notification document = null;
    String query = "FOR d IN notifications FILTER d.userId == @userId RETURN d";
    Map<String, Object> bindingVars = new MapBuilder().put("userId", userId).get();
    ArrayList<Notification> list = new ArrayList<Notification>();
    try {
      ArangoCursor<Notification> cursor =
          connection.db(dbName).query(query, bindingVars, null, Notification.class);
      while (cursor.hasNext()) {
        Notification notification = cursor.next();
        list.add(notification);
      }
    } catch (ArangoDBException exception) {
      exception.printStackTrace();
     // this.connection.shutdown();
    }
    //this.connection.shutdown();
    return list;
  }

  public void insertNormalNotification(String userId, String type) {
    insertNotification(userId, type, null, null, null);
  }

  public void insertDisccusionNotification(
      String userId, String type, String senderId, String discussionId) {
    insertNotification(userId, type, senderId, discussionId, null);
  }

  public void insertQuestionNotification(
      String userId, String type, String senderId, String questionId) {
    insertNotification(userId, type, senderId, null, questionId);
  }

  /** Insertion of a notificiation based on it's type. */
  public void insertNotification(
      String userId, String type, String senderId, String discussionId, String questionId) {
    boolean found = false;
    for (Type c : Type.values()) {
      if (c.name().equals(type)) {
        found = true;
      }
    }

    if (!found) {
      System.out.println("Notification type is invalid"); // TODO: Add Exception
      return;
    }
    try {
      BaseDocument doc = new BaseDocument();
      doc.addAttribute("userId", userId);
      doc.addAttribute("read", false);
      doc.addAttribute("type", type);
      if (discussionId != null) {
        doc.addAttribute("senderId", senderId);
        doc.addAttribute("discussionId", discussionId);
      }
      if (questionId != null) {
        doc.addAttribute("senderId", senderId);
        doc.addAttribute("questionId", questionId);
      }
      connection.db(dbName).collection("notifications").insertDocument(doc);
      //this.connection.shutdown();
    } catch (ArangoDBException exception) {
      exception.printStackTrace();
      //this.connection.shutdown();
    }
  }
}
