package com.gitrekt.quora.models;

import com.arangodb.entity.DocumentField;

public class Notification {
  public enum Type {
    new_follower,
    question_answered,
    question_starred,
    answer_starred,
    new_message,
    new_post,
    discussion_invitation,
    question_invitation,
    poll_answered;
  }

  @DocumentField(DocumentField.Type.KEY)
  String id;

  String userId;

  String senderId;

  String questionId;

  String discussionId;

  boolean read;

  Type type;

  public String getId() {
    return id;
  }

  public String getUserId() {
    return userId;
  }

  public String getSenderId() {
    return senderId;
  }

  public String getQuestionId() {
    return questionId;
  }

  public String getDiscussionId() {
    return discussionId;
  }

  public boolean getReadStatus() {
    return read;
  }

  public Type getType() {
    return type;
  }

  @Override
  public String toString() {
    return "Notification{"
        + "id='"
        + id
        + '\''
        + "userId='"
        + userId
        + '\''
        + "senderId='"
        + senderId
        + '\''
        + "discussionId='"
        + discussionId
        + '\''
        + "questionId='"
        + questionId
        + '\''
        + ", type='"
        + type
        + '\''
        + ", read="
        + read
        + '}';
  }
}
