package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;

import java.util.HashMap;

public class InsertNotificationsCommand extends Command {
  private static final String[] argumentNames = new String[] {"userId", "type"};

  public InsertNotificationsCommand(HashMap<String, Object> args) {
    super(args);
  }

  /** Execution of the insert notification command. */
  public String execute() {
    checkArguments(argumentNames);
    String userId = (String) args.get("userId");
    String senderId = (String) args.get("senderId");
    String discussionId = (String) args.get("discussionId");
    String questionId = (String) args.get("questionId");
    String type = (String) args.get("type");
    arangoHandler.insertNotification(userId, type, senderId, discussionId, questionId);
    return "Successfully Inserted";
  }
}
