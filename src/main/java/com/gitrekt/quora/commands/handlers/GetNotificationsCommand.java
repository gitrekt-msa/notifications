package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.models.Notification;

import java.util.ArrayList;
import java.util.HashMap;

public class GetNotificationsCommand extends Command {
  private static final String[] argumentNames = new String[] {"userId"};

  public GetNotificationsCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public ArrayList<Notification> execute() {
    checkArguments(argumentNames);
    String userId = (String) args.get("userId");
    ArrayList<Notification> res = arangoHandler.getUserNotifications(userId);
    return res;
  }
}
