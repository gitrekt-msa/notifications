package notifications_testing;

import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDB;
import com.arangodb.ArangoDBException;
import com.arangodb.entity.BaseDocument;
import com.arangodb.entity.CollectionEntity;
import com.arangodb.util.MapBuilder;
import com.gitrekt.quora.commands.handlers.InsertNotificationsCommand;
import com.gitrekt.quora.commands.handlers.GetNotificationsCommand;
import com.gitrekt.quora.database.arango.ArangoConnection;
import com.gitrekt.quora.database.arango.handlers.NotificationArangoHandler;
import com.gitrekt.quora.models.Notification;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/** Unit test for simple App. */
public class NotificationTest {
  /** Rigorous Test :-). */
  protected ArangoDB connection;

  protected String dbName;
  protected NotificationArangoHandler handler;

  @Before
  public void init() {
    connection = ArangoConnection.getInstance().getConnection();
    dbName = System.getenv("ARANGO_DB");
    handler = new NotificationArangoHandler();
    createDatabase();
    createCollection();
  }

  private void createDatabase() {
    Collection<String> dbs = connection.getDatabases();

    for (String db : dbs) {
      if (db.equals(dbName)) {
        return;
      }
    }

    try {
      connection.createDatabase(dbName);
    } catch (ArangoDBException exception) {
      exception.printStackTrace();
    }
  }

  private void createCollection() {
    Collection<CollectionEntity> collections = connection.db(dbName).getCollections();
    for (CollectionEntity entity : collections) {
      if (entity.getName().equals("notifications")) {
        connection.db(dbName).collection("notifications").drop();
      }
    }

    try {
      connection.db(dbName).createCollection("notifications");
    } catch (ArangoDBException exception) {
      exception.printStackTrace();
    }
  }

  private void insertNotification(String userId, boolean read, String type) {
    BaseDocument doc = new BaseDocument();

    doc.addAttribute("userId", userId);
    doc.addAttribute("read", read);
    doc.addAttribute("type", type);
    connection.db(dbName).collection("notifications").insertDocument(doc);
  }

  @Test
  public void checkNotificationIsCreatedNewFollower() {
    String userId = "12413543253723421312";
    insertNotification(userId, false, "new_follower");
    ArrayList<Notification> res = handler.getUserNotifications(userId);
    assertEquals("The notification document was not created", Integer.toString(res.size()), "1");
    assertEquals(
        "The notification document was not created", res.get(0).getType().name(), "new_follower");
    assertEquals("The notification document was not created", res.get(0).getReadStatus(), false);
  }

  @Test
  public void checkNotificationIsCreatedQuestionAnswered() {
    String userId = "124135432537234213312";
    insertNotification(userId, false, "question_answered");
    ArrayList<Notification> res = handler.getUserNotifications(userId);
    assertEquals("The notification document was not created", Integer.toString(res.size()), "1");
    assertEquals(
        "The notification document was not created",
        res.get(0).getType().name(),
        "question_answered");
    assertEquals("The notification document was not created", res.get(0).getReadStatus(), false);
  }

  @Test
  public void checkNotificationIsCreatedQuestionStarred() {
    String userId = "1241354325234213312";
    insertNotification(userId, false, "question_starred");
    ArrayList<Notification> res = handler.getUserNotifications(userId);
    assertEquals("The notification document was not created", Integer.toString(res.size()), "1");
    assertEquals(
        "The notification document was not created",
        res.get(0).getType().name(),
        "question_starred");
    assertEquals("The notification document was not created", res.get(0).getReadStatus(), false);
  }

  @Test
  public void checkNotificationIsCreatedAnswerStarred() {
    String userId = "1135432537234213312";
    insertNotification(userId, false, "answer_starred");
    ArrayList<Notification> res = handler.getUserNotifications(userId);
    assertEquals("The notification document was not created", Integer.toString(res.size()), "1");
    assertEquals(
        "The notification document was not created", res.get(0).getType().name(), "answer_starred");
    assertEquals("The notification document was not created", res.get(0).getReadStatus(), false);
  }

  @Test
  public void checkNotificationIsCreatedNewMessage() {
    String userId = "12413543253723312";
    insertNotification(userId, false, "new_message");
    ArrayList<Notification> res = handler.getUserNotifications(userId);
    assertEquals("The notification document was not created", Integer.toString(res.size()), "1");
    assertEquals(
        "The notification document was not created", res.get(0).getType().name(), "new_message");
    assertEquals("The notification document was not created", res.get(0).getReadStatus(), false);
  }

  @Test
  public void checkNotificationIsCreatedNewPost() {
    String userId = "12413547234213312";
    insertNotification(userId, false, "new_post");
    ArrayList<Notification> res = handler.getUserNotifications(userId);
    assertEquals("The notification document was not created", Integer.toString(res.size()), "1");
    assertEquals(
        "The notification document was not created", res.get(0).getType().name(), "new_post");
    assertEquals("The notification document was not created", res.get(0).getReadStatus(), false);
  }
  @Test
  public void checkNotificationIsCreatedDiscussion() {
    String userId = "12413547231231243312";
    insertNotification(userId, false, "discussion_invitation");
    ArrayList<Notification> res = handler.getUserNotifications(userId);
    assertEquals("The notification document was not created", Integer.toString(res.size()), "1");
    assertEquals(
            "The notification document was not created", res.get(0).getType().name(), "discussion_invitation");
    assertEquals("The notification document was not created", res.get(0).getReadStatus(), false);
  }
  @Test
  public void checkNotificationIsCreatedQuestion() {
    String userId = "12413547231231243312";
    insertNotification(userId, false, "question_invitation");
    ArrayList<Notification> res = handler.getUserNotifications(userId);
    assertEquals("The notification document was not created", Integer.toString(res.size()), "1");
    assertEquals(
            "The notification document was not created", res.get(0).getType().name(), "question_invitation");
    assertEquals("The notification document was not created", res.get(0).getReadStatus(), false);
  }
  @Test
  public void checkNotificationInsertion() {
    String userId = "84353412341";
    handler.insertNormalNotification(userId, "new_post");
    handler.insertNormalNotification(userId, "new_message");
    handler.insertNormalNotification(userId, "answer_starred");
    handler.insertNormalNotification(userId, "question_starred");
    handler.insertNormalNotification(userId, "question_answered");
    handler.insertNormalNotification(userId, "new_follower");
    handler.insertQuestionNotification(userId, "question_invitation","1243124123123","123123123123");
    handler.insertDisccusionNotification(userId, "discussion_invitation","13123123123","123123123");
    ArangoCursor<Notification> cursor =
        connection
            .db(dbName)
            .query(
                "FOR d IN notifications FILTER d.userId == @userId RETURN d",
                new MapBuilder().put("userId", userId).get(),
                null,
                Notification.class);
    ArrayList<Notification> list = new ArrayList<Notification>();
    while (cursor.hasNext()) {
      Notification notification = cursor.next();
      list.add(notification);
    }
    assertEquals("The Notifications for the user were not added", list.get(0).getUserId(), userId);
    assertEquals("Not all the Notifications were added", list.size(), 8);
  }

  @Test
  public void checkInsertNormalNotificationsCommand() {
    HashMap<String, Object> map = new HashMap<>();
    String userId = "12413543253723421312";
    map.put("userId", userId);
    map.put("type", "new_message");
    InsertNotificationsCommand getnot = new InsertNotificationsCommand(map);
    getnot.setArangoHandler(new NotificationArangoHandler());
    getnot.execute();
    ArangoCursor<Notification> cursor =
        connection
            .db(dbName)
            .query(
                "FOR d IN notifications FILTER d.userId == @userId RETURN d",
                new MapBuilder().put("userId", userId).get(),
                null,
                Notification.class);
    ArrayList<Notification> list = new ArrayList<Notification>();
    while (cursor.hasNext()) {
      Notification notification = cursor.next();
      list.add(notification);
    }
    assertEquals("The Notifications for the user were not added", list.get(0).getUserId(), userId);
  }

  @Test
  public void checkInsertPollNotificationsCommand() {
    HashMap<String, Object> map = new HashMap<>();
    String userId = "1241354325372342aa2";
    map.put("userId", userId);
    map.put("type", "poll_answered");
    InsertNotificationsCommand getnot = new InsertNotificationsCommand(map);
    getnot.setArangoHandler(new NotificationArangoHandler());
    getnot.execute();
    ArangoCursor<Notification> cursor =
            connection
                    .db(dbName)
                    .query(
                            "FOR d IN notifications FILTER d.userId == @userId RETURN d",
                            new MapBuilder().put("userId", userId).get(),
                            null,
                            Notification.class);
    ArrayList<Notification> list = new ArrayList<Notification>();
    while (cursor.hasNext()) {
      Notification notification = cursor.next();
      list.add(notification);
    }
    assertEquals("The Notifications for the user were not added", list.get(0).getUserId(), userId);
    assertEquals("The Notifications type was not added properly", list.get(0).getType().name(), "poll_answered");
  }

  @Test
  public void checkInsertQuestionNotificationsCommand() {
    HashMap<String, Object> map = new HashMap<>();
    String userId = "1241354325372342aa2";
    map.put("userId", userId);
    map.put("type", "question_invitation");
    map.put("senderId","123123123");
    map.put("questionId","123123123123");
    InsertNotificationsCommand getnot = new InsertNotificationsCommand(map);
    getnot.setArangoHandler(new NotificationArangoHandler());
    getnot.execute();
    ArangoCursor<Notification> cursor =
            connection
                    .db(dbName)
                    .query(
                            "FOR d IN notifications FILTER d.userId == @userId RETURN d",
                            new MapBuilder().put("userId", userId).get(),
                            null,
                            Notification.class);
    ArrayList<Notification> list = new ArrayList<Notification>();
    while (cursor.hasNext()) {
      Notification notification = cursor.next();
      list.add(notification);
    }
    assertEquals("The Notifications for the user were not added", list.get(0).getUserId(), userId);
    assertEquals("The Notifications type was not added properly", list.get(0).getType().name(), "question_invitation");
    assertEquals("The Notifications question Id was not added properly", list.get(0).getQuestionId(),"123123123123");
    assertEquals("The Notifications sender Id was not added properly", list.get(0).getSenderId(),"123123123");
 }
  @Test
  public void checkInsertDiscussionNotificationsCommand() {
    HashMap<String, Object> map = new HashMap<>();
    String userId = "12413543253723421312sef";
    map.put("userId", userId);
    map.put("type", "discussion_invitation");
    map.put("senderId","123123123");
    map.put("questionId","123123123123");
    InsertNotificationsCommand getnot = new InsertNotificationsCommand(map);
    getnot.setArangoHandler(new NotificationArangoHandler());
    getnot.execute();
    ArangoCursor<Notification> cursor =
            connection
                    .db(dbName)
                    .query(
                            "FOR d IN notifications FILTER d.userId == @userId RETURN d",
                            new MapBuilder().put("userId", userId).get(),
                            null,
                            Notification.class);
    ArrayList<Notification> list = new ArrayList<Notification>();
    while (cursor.hasNext()) {
      Notification notification = cursor.next();
      list.add(notification);
    }
    assertEquals("The Notifications for the user were not added", list.get(0).getUserId(), userId);
    assertEquals("The Notifications type was not added properly", list.get(0).getType().name(), "discussion_invitation");
    assertEquals("The Notifications question Id was not added properly", list.get(0).getQuestionId(),"123123123123");
    assertEquals("The Notifications sender Id was not added properly", list.get(0).getSenderId(),"123123123");
  }
    @Test
    public void checkGetNotificationsCommand() {
      String userId = "21312412312aaaa3";
      insertNotification(userId, false, "new_post");
      HashMap<String, Object> map = new HashMap<>();
      map.put("userId", userId);
      GetNotificationsCommand command = new GetNotificationsCommand(map);
      command.setArangoHandler(new NotificationArangoHandler());
      ArrayList<Notification> res = command.execute();
      assertEquals("The Notifications for the user were not added", res.get(0).getUserId(),
   userId);
      assertEquals("Not the right notification was added", res.get(0).getType().name(),
   "new_post");
    }
}
